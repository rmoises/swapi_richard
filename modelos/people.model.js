{
    "type": "object",
    "properties": {
        "nombre": {
            "type": "string"
        },
        "altura": {
            "type": "string"
        },
        "masa": {
            "type": "string"
        },
        "color_cabello": {
            "type": "string"
        },
        "color_piel": {
            "type": "string"
        },
        "color_ojo": {
            "type": "string"
        },
        "anio_nacimiento": {
            "type": "string"
        },
        "genero": {
            "type": "string"
        },
        "planeta_origen": {
            "type": "string"
        },
        "creado": {
            "type": "string"
        },
        "editado": {
            "type": "string"
        },
        "url": {
            "type": "string"
        }
    }
}