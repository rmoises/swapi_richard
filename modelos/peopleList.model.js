{
    "type": "object",
    "properties": {
        "contador": {
            "type": "integer"
        },
        "siguiente": {
            "type": "string"
        },
        "anterior": {
            "type": "string"
        },
        "resultados": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "nombre": {
                        "type": "string"
                    },
                    "altura": {
                        "type": "string"
                    },
                    "masa": {
                        "type": "string"
                    },
                    "color_cabello": {
                        "type": "string"
                    },
                    "color_piel": {
                        "type": "string"
                    },
                    "color_ojo": {
                        "type": "string"
                    },
                    "anio_nacimiento": {
                        "type": "string"
                    },
                    "genero": {
                        "type": "string"
                    },
                    "planeta_origen": {
                        "type": "string"
                    },
                    "peliculas": {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    },
                    "especies": {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    },
                    "veiculos": {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    },
                    "naves_espaciales": {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    },
                    "creado": {
                        "type": "string"
                    },
                    "editado": {
                        "type": "string"
                    },
                    "url": {
                        "type": "string"
                    }
                }
            }
        }
    }
}