{
    "type": "object",
    "properties": {
        "people": {
            "type": "string"
        },
        "planets": {
            "type": "string"
        },
        "films": {
            "type": "string"
        },
        "species": {
            "type": "string"
        },
        "vehicles": {
            "type": "string"
        },
        "starships": {
            "type": "string"
        }
    }
}