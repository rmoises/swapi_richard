'use strict';

var AWS = require('aws-sdk');

var dynamo = new AWS.DynamoDB();

exports.handler = function(event, context, callback) {

    var params = {
        TableName: 'persona_tbl',
        Select: 'ALL_ATTRIBUTES'// ,
        // FilterExpression: '#T = :id',
        // ExpressionAttributeNames: { '#T': 'persona_id' },
        // ExpressionAttributeValues: { ':id': { S: '400' } }
    };

    var lista = [];
    
    onScan(params, lista, callback);

};


function onScan(params, lista, callback) {

    dynamo.scan(params, function(err, data) {
        if (err === null) {

            var participants = '';
            var average_height = '';
            var average_lifespan = '';
            var created = '';
            var name = '';

            data.Items.forEach(function(item) {

                lista.push({
                    'nombre': item.nombre.S,
                    'altura': item.altura.S,
                    'masa': item.masa.S,
                    'color_cabello': item.color_cabello.S,
                    'color_piel': item.color_piel.S,
                    'color_ojo': item.color_ojo.S,
                    'anio_nacimiento': item.anio_nacimiento.S,
                    'genero': item.genero.S,
                    'planeta_origen': item.planeta_origen.S,
                    'peliculas': item.peliculas.S,
                    'especies': item.especies.S,
                    'veiculos': item.veiculos.S,
                    'naves_espaciales': item.naves_espaciales.S,
                    'creado': item.creado.S,
                    'editado': item.editado.S,
                    'url': item.url.S
                });
            });

            // continue scanning if we have more items
            if (typeof data.LastEvaluatedKey != "undefined") {
                // console.log("Scanning for more...");
                params.ExclusiveStartKey = data.LastEvaluatedKey;
                onScan(params, lista, callback);
            }
            else {
                callback(null, lista);
            }
        }
        else {
            callback(err);
        }
    });
}