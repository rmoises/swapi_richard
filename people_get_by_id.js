'use strict';

exports.handler = function(event, context, callback) {
    const fetch = require('node-fetch');

    fetch('https://swapi.py4e.com/api/people/'+event.id+'/').then((res) => {
        // console.log(res);
        return res.json();
    }).then((obj) => {
        // console.log(json);
        
        var perso = {}
        
        perso['nombre'] = obj['name'];
        perso['altura'] = obj['height'];
        perso['masa'] = obj['mass'];
        perso['color_cabello'] = obj['hair_color'];
        perso['color_piel'] = obj['skin_color'];
        perso['color_ojo'] = obj['eye_color'];
        perso['anio_nacimiento'] = obj['birth_year'];
        perso['genero'] = obj['gender'];
        perso['planeta_origen'] = obj['homeworld'];
        perso['peliculas'] = obj['films'];
        perso['especies'] = obj['species'];
        perso['veiculos'] = obj['vehicles'];
        perso['naves_espaciales'] = obj['starships'];
        perso['creado'] = obj['created'];
        perso['editado'] = obj['edited'];
        perso['url'] = obj['url'];
        
        callback(null, obj);
    });
}