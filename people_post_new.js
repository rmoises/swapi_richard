'use strict';

var AWS = require('aws-sdk');

var dynamo = new AWS.DynamoDB();

exports.handler = function(event, context, callback) {


    const done = function(err, res) {
        err ? callback(null, err) : callback(null, res);
    };

    var jsonObj = {};
    
     callback(null, event.persona);
    
    var arrayFilas = event.persona.replace('{', "").replace('}', "").replace(/(\r\n|\n|\r)/gm, "").split(", ");
    
    for(const fila of arrayFilas) {
        var elemnt = fila.split("=");
        jsonObj[elemnt[0]] = elemnt[1];
    }
    
    guardarPersona(jsonObj,done);
};


function guardarPersona(persona,callback) {
    
    dynamo.putItem({
        TableName: 'persona_tbl',
        Item: {
            persona_id: {N: "" + new Date().getTime()},
            nombre: {S: "" + persona['nombre']},
            altura: {S: "" + persona['altura']},
            masa: {S: "" + persona['masa']},
            color_cabello: {S: "" + persona['color_cabello']},
            color_piel: {S: "" + persona['color_piel']},
            color_ojo: {S: "" + persona['color_ojo']},
            anio_nacimiento: {S: "" + persona['anio_nacimiento']},
            genero: {S: "" + persona['genero']},
            planeta_origen: {S: "" + persona['planeta_origen']},
            peliculas: {S: "" + persona['peliculas']},
            especies: {S: "" + persona['especies']},
            veiculos: {S: "" + persona['veiculos']},
            naves_espaciales: {S: "" + persona['naves_espaciales']},
            creado: {S: "" + persona['creado']},
            editado: {S: "" + persona['editado']},
            url: {S: "" + persona['url']}
        }
    }, callback);
}