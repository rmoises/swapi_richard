'use strict';

exports.handler = function (event, context, callback) {
    const fetch = require('node-fetch');
    
    fetch('https://swapi.py4e.com/api/').then((res) => {
        // console.log(res);
        return res.json();
    }).then((json) => {
        
        // callback(null, json);
        
        var resultado = {};
        resultado['personas'] = json['people'];
        resultado['planetas'] = json['planets'];
        resultado['peliculas'] = json['films'];
        resultado['especies'] = json['species'];
        resultado['vehiculos'] = json['vehicles'];
        resultado['naves'] = json['starships'];
        
        callback(null, resultado);
    });
}